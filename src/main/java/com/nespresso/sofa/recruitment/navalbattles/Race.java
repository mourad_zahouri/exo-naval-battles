package com.nespresso.sofa.recruitment.navalbattles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Race {
	
	private List<Ship> ships = new ArrayList<Ship>();

	public Race(Ship... ships) {
		for (Ship ship : ships) {
			this.ships.add(ship);
		}
	}

	public Ship winner() {
		Collections.sort(ships);
		Ship fasterShip = ships.get(0);
		return fasterShip;
	}

}
