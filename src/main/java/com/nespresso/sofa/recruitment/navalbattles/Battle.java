package com.nespresso.sofa.recruitment.navalbattles;

import java.util.ArrayList;
import java.util.List;


public class Battle {
	
	public static final Boolean LOCALIZED_DAMAGES = Boolean.TRUE;
	
	private Ship firstSideShip;
	private List<Ship> secondSide = new ArrayList<Ship>();
	private List<Ship> winningSide = new ArrayList<Ship>();
	
	private ShipAttackMode attackMode;
	private boolean isLocalizedDamagesMode;

	public Battle() {
		this(false);
	}
	
	public Battle(Boolean localizedDamages) {
		isLocalizedDamagesMode = localizedDamages;
		if(isLocalizedDamagesMode){
			attackMode = new ShipLocalizedDamageAttackMode();
		}else{
			attackMode = new ShipNormalAttackMode();
		}
	}

	public Battle side(Ship firstSide) {
		this.firstSideShip = firstSide;
		return this;
	}

	public Battle against(Ship... secondSide) {
		createBattleSecondeSide(secondSide);
		runBattle();
		designateWinners();
		return this;
	}

	private void createBattleSecondeSide(Ship... secondSide) {
		for (Ship ship : secondSide) {
			this.secondSide.add(ship);
		}
	}

	private void runBattle() {
		while(firstSideShip.isAlive() && isSecondSideAlive()){
			firstSideAttack();
			secondeSideAttack();
		}
	}

	private void designateWinners() {
		if(firstSideShip.isAlive()){
			winningSide.add(firstSideShip);
		}else{
			winningSide.addAll(secondSide);
		}
	}

	private void secondeSideAttack() {
		for (Ship ship : secondSide) {
			if(ship.isAlive()){
				int numberOfAliveAllies = calculateAliveAllies();
				attackMode.performAttack(ship, firstSideShip, numberOfAliveAllies);
			}
		}
	}

	private int calculateAliveAllies() {
		int numberOfAliveAllies = 0;
		for(int i = 0 ; i < secondSide.size() ; i++){
			if(secondSide.get(i).isAlive()){
				numberOfAliveAllies++;
			}
		}
		return numberOfAliveAllies - 1;
	}

	private void firstSideAttack() {
		Ship target = pickUpAliveTargetFromSecondSide();
		attackMode.performAttack(firstSideShip, target, 0);
	}
	
	private Ship pickUpAliveTargetFromSecondSide() {
		for(int i = 0 ; i < secondSide.size() ; i++){
			if(secondSide.get(i).isAlive()){
				return secondSide.get(i);
			}
		}
		return null;
	}

	private boolean isSecondSideAlive(){
		return pickUpAliveTargetFromSecondSide() != null;
	}

	public Boolean isInTheWinningSide(Ship ship) {
		return winningSide.contains(ship);
	}

}
