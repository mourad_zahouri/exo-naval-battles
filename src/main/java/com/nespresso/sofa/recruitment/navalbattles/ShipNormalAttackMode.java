package com.nespresso.sofa.recruitment.navalbattles;

public class ShipNormalAttackMode implements ShipAttackMode {

	@Override
	public void performAttack(Ship attacker, Ship atteckedShip, Integer numberOfAliveAllies) {
		Double totalDamage = attacker.caldulateCausedDamage(numberOfAliveAllies);
		atteckedShip.reduceHitPointBy(totalDamage);
	}

}
