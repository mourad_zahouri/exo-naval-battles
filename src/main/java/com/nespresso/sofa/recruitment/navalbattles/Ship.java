package com.nespresso.sofa.recruitment.navalbattles;

public class Ship implements Comparable<Ship> {
	private static final Integer DAMAGE_PER_CANON = 200;
	private static final int HIT_POINTS_PER_MAST = 1000;
	private static final int HIT_POINTS_PER_CANON = 100;

	protected TonsOfDisplacementByMastCoefficient coefficient;
	
	private Double hitPoints;
	private Integer canonsNumber;
	private Integer tonsOfDisplacement;
	private Integer mastNumber;
	
	public Ship(Integer tonsOfDisplacement, Integer mastNumber) {
		this(tonsOfDisplacement, mastNumber, 0);
	}
	
	public Ship(Integer tonsOfDisplacement, Integer mastNumber, Integer canonsNumber) {
		this.tonsOfDisplacement = tonsOfDisplacement;
		this.mastNumber = mastNumber;
		this.canonsNumber = canonsNumber;
		coefficient = new TonsOfDisplacementByMastCoefficient(tonsOfDisplacement, mastNumber, canonsNumber);
		hitPoints = calculateHitPoints(tonsOfDisplacement, mastNumber, canonsNumber);
	}
	
	private static Double calculateHitPoints(Integer tonsOfDisplacement, Integer mastNumber, Integer canonsNumber) {
		Double points = (double)tonsOfDisplacement + HIT_POINTS_PER_CANON * canonsNumber + HIT_POINTS_PER_MAST * mastNumber;
		return points;
	}
	
	public Boolean isAlive(){
		return this.hitPoints > 0 ;
	}

	public void reduceHitPointBy(Double points){
		this.hitPoints -= points;
	}
	
	public void applyDamage(Double damagePoint){
		double points = damagePoint.doubleValue();
		points = adjustMastsNumber(points);
		points = adjustCanonNumber(points);
		adjustTonsOfDisplacement(points);
		hitPoints = calculateHitPoints(tonsOfDisplacement, mastNumber, canonsNumber);
	}

	private void adjustTonsOfDisplacement(double points) {
		if(tonsOfDisplacement > 0){
			tonsOfDisplacement -= (int)points;
		}
	}

	private double adjustCanonNumber(double points) {
		while(canonsNumber > 0 && points > HIT_POINTS_PER_CANON){
			canonsNumber--;
			points-= HIT_POINTS_PER_CANON;
		}
		return points;
	}

	private double adjustMastsNumber(double points) {
		while(mastNumber > 0 && points > HIT_POINTS_PER_MAST){
			mastNumber--;
			points-= HIT_POINTS_PER_MAST;
		}
		return points;
	}
	
	public Double caldulateCausedDamage(Integer numberOfAliveAllies){
		Integer damage =  DAMAGE_PER_CANON * canonsNumber;
		Double bonus = damage * (numberOfAliveAllies * 0.15);
		return damage + bonus;
	}

	@Override
	public int compareTo(Ship o) {
		return this.coefficient.compareTo(o.coefficient);
	}

}
