package com.nespresso.sofa.recruitment.navalbattles;

public class ShipLocalizedDamageAttackMode implements ShipAttackMode {

	@Override
	public void performAttack(Ship attacker, Ship atteckedShip, Integer numberOfAliveAllies) {
		Double totalDamage = attacker.caldulateCausedDamage(numberOfAliveAllies);
		atteckedShip.applyDamage(totalDamage);
	}

}
