package com.nespresso.sofa.recruitment.navalbattles;

public class Clipper extends Ship {
	
	private static final Double CLIPPER_BONUS = 0.2d; 
	
	public Clipper(Integer tonsOfDisplacement, Integer mastNumber) {
		super(tonsOfDisplacement, mastNumber);
		this.coefficient.applyBonus(1 - CLIPPER_BONUS);
	}
}
