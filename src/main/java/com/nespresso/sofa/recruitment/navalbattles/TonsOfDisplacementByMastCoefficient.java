package com.nespresso.sofa.recruitment.navalbattles;

public class TonsOfDisplacementByMastCoefficient implements Comparable<TonsOfDisplacementByMastCoefficient> {
	
	private static final Double PENALTY_PER_CANON = 0.005D;
	
	private Double coefficientValue;
	
	public TonsOfDisplacementByMastCoefficient(Integer tonsOfDisplacement, Integer mastNumber, Integer canonsNumber) {
		coefficientValue = calculateValue(tonsOfDisplacement, mastNumber, canonsNumber);
	}
	
	private static double calculateValue(Integer tonsOfDisplacement, Integer mastNumber, Integer canonsNumber) {
		Double value = tonsOfDisplacement / (double)mastNumber;
		Double canonsPenalty = calculateCanonsPenalty(canonsNumber);
		value *= canonsPenalty;
		
		return value;
	}
	
	private static double calculateCanonsPenalty(int canonsNumber) {
		return 1 + canonsNumber * PENALTY_PER_CANON;
	}
	
	public void applyBonus(double bonus){
		coefficientValue *= bonus;
	}

	@Override
	public int compareTo(TonsOfDisplacementByMastCoefficient o) {
		return this.coefficientValue.compareTo(o.coefficientValue);
	}
}
