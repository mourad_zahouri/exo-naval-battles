package com.nespresso.sofa.recruitment.navalbattles;

public interface ShipAttackMode {

	void performAttack(Ship attacker, Ship atteckedShip, Integer numberOfAliveAllies);
}
